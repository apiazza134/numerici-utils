import yaml
import re
import os
import numpy as np
import matplotlib.pyplot as plt


def yaml_load(filename: str) -> dict:
    """Wrapper aroud yaml.safe_load
    """
    loader = yaml.SafeLoader
    loader.add_implicit_resolver(
        u'tag:yaml.org,2002:float',
        re.compile(u'''^(?:
     [-+]?(?:[0-9][0-9_]*)\\.[0-9_]*(?:[eE][-+]?[0-9]+)?
    |[-+]?(?:[0-9][0-9_]*)(?:[eE][-+]?[0-9]+)
    |\\.[0-9_]+(?:[eE][-+][0-9]+)?
    |[-+]?[0-9][0-9_]*(?::[0-5]?[0-9])+\\.[0-9_]*
    |[-+]?\\.(?:inf|Inf|INF)
    |\\.(?:nan|NaN|NAN))$''', re.X),
        list(u'-+0123456789.'))

    with open(filename, 'r') as f:
        return yaml.load(f, Loader=loader)


def get_parameters(filename: str) -> dict:
    """Read first commented lines in a data file as a yaml and output a
    dictionary of the parametes
    """

    yaml_header = ''
    with open(filename, "r") as f:
        line = f.readline()
        while line[0] == "#":
            yaml_header += line[2:]
            line = f.readline()

    return yaml.safe_load(yaml_header.strip('# '))


def blocking(x: np.array, block: int) -> np.array:
    """Apply blocking transformation to x with block size block
    """
    size = int(len(x) / block)
    y = np.empty(size)

    for i in range(size):
        y[i] = np.mean(x[(i * block):(i * block + block)])

    return y


def block_optimize(x: np.array, max_block_size: int, block_lin_step = 100, return_fig = False):
    """Plot standard error estimate with blocking for variable block size to
    choose an approximate optimal block size
    """
    blocks = np.arange(1, max_block_size, block_lin_step)
    sigmaE_mean_x = []
    for block_size in blocks:
        print(f"{block_size=}")
        x_blocked = blocking(x, block_size)
        sigmaE_mean_x.append(mean_std(x_blocked))

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.plot(blocks, sigmaE_mean_x, color='blue')
    plt.show()
    if return_fig:
        return fig
    else:
        plt.close()


def bootstrap(data_arr: np.array, estimator, n_resample: int = None,
              sample_size: int = None, block_size: int = 1):
    """Compute error on a non-trivial esitimator using the bootstrap technique
    """
    N = len(data_arr)

    # Default values
    if n_resample is None:
        n_resample = N
    if sample_size is None:
        sample_size = N

    rng = np.random.default_rng()

    estimator_vals = []
    for _ in range(n_resample):
        resample_idx = rng.integers(low=0, high=N, size=int(sample_size / block_size))
        resample_idx = np.array([np.arange(r, r + block_size) for r in resample_idx]).flatten()
        resample_idx = resample_idx[resample_idx < sample_size]
        resample = data_arr[resample_idx]

        estimator_vals.append(estimator(resample))

    return np.array(estimator_vals)


def connected_time_correlation(x: np.array, max_time, normalized=False) -> np.array:
    if max_time > len(x) - 1:
        raise IndexError

    x_mean = x.mean()
    C = np.array(
        [(x * x).mean() - x_mean**2] +
        [(x[:-k] * x[k:]).mean() - x_mean**2 for k in range(1, max_time)]
    )

    if normalized:
        if C[0] != 0:
            return C / C[0]
        else:
            print("C[0] is zero, returning unormalized correlations")

    return C


def proper_divisors(n: int) -> [int]:
    """Compute proper divisors of n
    """
    divisors = []
    for i in range(1, int(n/2) + 1):
        if n % i == 0:
            divisors.append(i)

    return divisors


def std(x: np.array) -> float:
    """Compute the unbiased standard deviation of x. The values in x are
    assumed to be uncorrelated
    """
    return np.std(x, ddof=1)


def mean_std(x: np.array) -> float:
    """Compute the standard deviation of the mean of x. The values in x are
    assumed to be uncorrelated
    """
    return std(x) / np.sqrt(len(x))


def style_use():
    """Sets plot style in all subsequents plots
    """
    style = {
        'text.usetex': True,
        'text.latex.preamble': r'\usepackage[euler-digits]{eulervm}',
        'font.family': 'serif',
        'font.size': 15,
        'axes.titlesize': 17,
        'axes.labelsize': 17,
        'lines.linewidth': 0.5,
        'savefig.dpi': 600,
        'savefig.bbox': 'tight'
    }
    plt.style.use(style)


def plot(x, y, xerr=None, yerr=None, xlabel: str = None, ylabel: str = None,
         title: str = None, save_fname: str = None, show: bool = False,
         **plt_kwargs):
    """Wrapper around plt.errorbar (if either one of the two
    errors is not None). Also set figure style, but this will affect
    all subsequents plots as a side effect: to restore the normal plot
    style use plt.style.use('classic') after calling this function.
    """
    style_use()

    plt.errorbar(x, y, xerr=xerr, yerr=yerr, **plt_kwargs)

    if xlabel:
        plt.xlabel(xlabel)
    if ylabel:
        plt.ylabel(ylabel)
    if title:
        plt.title(title)
    if save_fname:
        plt.savefig(save_fname)
    if show:
        plt.show()

    plt.close()


def plot_with_yerrors(x, y, xerr=None, yerr=None, xlabel: str = None,
                      ylabel: str = None, title: str = None, save_fname: str =
                      None, show: bool = False, **plt_kwargs):
    """Like `utils.plot` but will also display (x, yerr) just below the
    (x, y) graph.

    """
    style_use()

    fig, ax = plt.subplots(2, 1, sharex=True,
                           gridspec_kw={'hspace': 0, 'height_ratios': [3, 1]})
    ax[0].errorbar(x, y, xerr=xerr, yerr=yerr, **plt_kwargs)
    ax[1].plot(x, yerr, linestyle='', marker='x', markersize=2, color='black')

    if xlabel:
        ax[1].set_xlabel(xlabel)
    if ylabel:
        ax[0].set_ylabel(ylabel)
    if title:
        ax[0].set_title(title)
    if save_fname:
        plt.savefig(save_fname)
    if show:
        plt.show()

    plt.close()


def title_from_config(data_fname: str, *args, before_params: str = None) -> str:
    config = get_parameters(data_fname)

    title = [before_params] if before_params else []
    for key in args:
        if key in config:
            title.append(
                f'${key} = {config[key]}$'
            )
        else:
            raise KeyError("{key} not found in {config_fname}.")

    return ', '.join(title)


def write_arrays(arrays: (np.array), fname: str, header: str = ''):
    """Wrapper around `np.savetxt`. Puts every element of arrays as a
    column.  Also creates eventual non existing directory in `fname`.
    """
    towrite = np.stack(arrays, axis=1)

    directory, filename = os.path.split(fname)
    os.makedirs(directory, exist_ok=True)

    np.savetxt(fname, towrite, header=header)

def chisq(xs: (np.array), ys: (np.array), yerrs: (np.array), model, fit_params):
    return np.sum(((ys - model(xs, *fit_params)) / yerrs) ** 2)